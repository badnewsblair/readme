## Blair's README

**Blair Christopher, Product Design Manager, Manage & Plan** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 


## Related pages

- [GitLab](https://gitlab.com/badnewsblair)
- [LinkedIn]( www.linkedin.com/in/badnewsblair)


## About me

- I grew up in [Richmond, Virginia USA](https://en.wikipedia.org/wiki/Richmond%2C_Virginia). Aside from the 4 years I spent at [college](https://www2.gmu.edu/), I've lived here my whole life. I love this small city and am happy to call it home!
- I have 3 kids. My oldest daughter is 12 and I have twin 6 year olds (a boy and a girl).
- I've been in design for well over a decade.
- I spent 14 years at my last company.
- I love good coffee, bad coffee and everything in between!
- I started playing guitar at age 14.
- I'm a gamer. No consoles please. I play games with a mouse and keyboard. I've made every gaming PC I've owned as well as my home server and router. 
- If I wasn't in Design, I'd probably be in Networking.
- I'm a Mechanical Keyboard Enthusiast. I built my keyboard too!
- I'm a big fan of Open Source and contribute to a [few projects](https://pi-hole.net/).


## My working style

- I feed off of collaboration! I do my best work with others.
- I can be *long-winded* (sychronous and asynchrous).
- I'm working on being more succinct and giving others space to contribute. Please feel free to help me with this!
- I live by my [calendar](https://flexibits.com/fantastical) and [Todo](https://culturedcode.com/things/) list!
- I believe we cannot do this without each other.
- I'm first to apologize and first to admit when I'm wrong.

## My leading style

- I believe in the saying "A rising tide raises all ships". I don't succeed unless my team does. There is no me, just we.
- I describe my managment style as "flexible". I can micro-manage if it is needed, but I'd rather spend my time enabling, focusing and inspiring.
- I'm here to create great work and connect with great people.
- I strive for my Designers to have Ownership (captial "O") of their domain.


## What I assume about others

- I always assume you know more than me.
- I always assume positive intent. Feedback is welcome!
- We're all here for the same reasons. No ulterior motives.


## Communicating with me

- Slack is the best way to get in touch with me. Otherwise, tag me. Email is always an option, but I tend to stay out of my Inbox.
- My normal working hours are usuall 10 AM to 6 PM (EST). Every day is a little different, however my mornings are my power hours as I start to get distracted as the family returns home for the day.
- I learn by doing and I "drink from the firehose". Don't hold back when teaching me. I've been doing this for a while and can filter myself.

